import os

class Menu:
    """Classe abstraite de tous les menus du projet."""

    def clear():
        os.system('cls' if os.name=='nt' else 'clear')

    def choix_binaire(affichage):
        a = 0
        while a not in ['o','n','O','N']:
            a = input(affichage)
        return a in ['o','O']

    def input_int(affichage):
        while True:
            try:
                return int(input(affichage))
            except Exception as e:
                print("Mauvaise valeur")

    def input_float(affichage):
        while True:
            try:    
                return float(input(affichage))
            except Exception as e:
                print("Mauvaise valeur")

    def input_plaque():
        plaque = ""
        while (len(plaque)!=7 or ord(plaque[0])<65 or ord(plaque[1]) < 65 or ord(plaque[0])>91 or ord(plaque[1]) > 91 or int(plaque[2:5]) < 1 or int(plaque[2:5]) > 999 or ord(plaque[5])<65 or ord(plaque[6]) < 65 or ord(plaque[5])>91 or ord(plaque[6]) > 91):
            if plaque == "BREAK":
                break
            if plaque != "":
                print("Plaque invalide : aa111aa")
            try:
                plaque = input("Saisir la plaque du vehicule : (break pour sortir) ").upper()
            except Exception as e:
                print("Mauvaise valeur")
        return plaque

    def input_permis():
        numero_permis=[]
        while len(numero_permis)>25 or len(numero_permis) == 0:
            try:
                numero_permis = input("Saisir le numero de permis : ").upper()
            except Exception as e:
                print("Mauvaise valeur")
        return numero_permis

    def input_numero_tel():
        numero_tel = ""
        while not numero_tel.isnumeric() or len(numero_tel)>11 or len(numero_tel)<10:
            numero_tel = input("Saisissez le numero de telephone  : ")
        return numero_tel

    def input_ddn():
        ddn = ""
        while len(ddn)!=10 or int(ddn[0:4])<1900 or ddn[4] !='-' or int(ddn[5:7])<1 or int(ddn[5:7])>12 or ddn[7] != '-' or int(ddn[8:])>31:
            ddn = input("Saisissez la date de naissance (YYYY-MM-JJ) : ")
        return ddn

    def input_date(affichage):
        d = ""
        while len(d)!=10:
            d = input(affichage)
        return(d)


    def input_cb(affichage):
        cb = ""
        while len(cb)!=16:
            cb = input(affichage)
        return(cb)
