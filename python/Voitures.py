from BDD import BDD
from tabulate import tabulate
from Menu import Menu

def Parc_voiture():
    parc_voiture()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def parc_voiture():
    voitures_sql = "SELECT * FROM vehicule ORDER BY numero_immat"
    if not BDD().execute(voitures_sql):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures dans le parc de location ----")
    voitures.insert(0,("numero_immat","couleur","nbkm","tarif","modele","typecarbu","loue","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
    
def Voitures_dispo():
    voitures_dispo()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def voitures_dispo():
    voitures_dispo_sql = "SELECT numero_immat,couleur,tarif,modele,typeCarbu,agence \
    FROM vehicule WHERE loue = FALSE ORDER BY numero_immat"
    if not BDD().execute(voitures_dispo_sql):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures disponibles a la location ----")
    voitures.insert(0,("numero_immat","couleur","tarif","modele","typecarbu","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
   
def Voitures_louees():
    voitures_louees()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def voitures_louees():
    voitures_louees_sql = "SELECT numero_immat,couleur,tarif,modele,typeCarbu,agence \
    FROM vehicule WHERE loue = TRUE ORDER BY numero_immat"
    if not BDD().execute(voitures_louees_sql):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures actuellement louees ----")
    voitures.insert(0,("numero_immat","couleur","tarif","modele","typecarbu","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
    
def Voiture_plaque():
    voiture_plaque()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def voiture_plaque():
    plaque = Menu.input_plaque()
    voiture = "SELECT numero_immat,couleur,tarif,modele,typeCarbu,loue,agence \
    FROM vehicule WHERE numero_immat = '%s' " %(plaque)
    if not BDD().execute(voiture):
        return;
    voitures = BDD().fetchall()
    print("---- Voiture recherchee ----")
    voitures.insert(0,("numero_immat","couleur","tarif","modele","typecarbu","loue","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))

def Voiture_option():
    voiture_option()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def voiture_option():
    parc_voiture()
    plaque = Menu.input_plaque()
    option = "SELECT option_v FROM Option_vehicule WHERE vehicule = '%s' " %(plaque)
    if not BDD().execute(option):
        return
    options = BDD().fetchall()
    print("--- Options de la voiture %s ---" %(plaque))
    options.insert(0,("Options",))
    print(tabulate(options,headers='firstrow',tablefmt='psql'))
    

    
def Voiture_modele():
    voiture_modele()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def voiture_modele():
    sql_modele = "SELECT * FROM Modele ORDER BY nom;"
    if not BDD().execute(sql_modele):
        return;
    modele = BDD().fetchall()
    print("---- Les differentes modeles ----")
    modele.insert(0,("nom","marque","nbPorte","categorie"))
    print(tabulate(modele,headers='firstrow',tablefmt='psql'))
    
    modele = input("\nSaisir le modele de vehicule : ")
    voiture_modele = "SELECT numero_immat,couleur,tarif,modele,typeCarbu,loue,agence \
    FROM vehicule WHERE modele LIKE '{}{}' ORDER BY numero_immat".format(modele,'%')
    if not BDD().execute(voiture_modele):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures recherchees ----")
    voitures.insert(0,("numero_immat","couleur","tarif","modele","typecarbu","loue","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
    
def Voiture_categorie():
    voiture_categorie()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def voiture_categorie():
    categ_sql = "SELECT * FROM Categorie"
    if not BDD().execute(categ_sql):
        return;
    categ = BDD().fetchall()
    print("---- Categories disponibles ----")
    categ.insert(0,("categorie",))
    print(tabulate(categ,headers='firstrow',tablefmt='psql'))

    categ_choisie = input("Saisir la categorie de vehicule : ")
    voiture_modele = "SELECT numero_immat,couleur,tarif,modele,typeCarbu,loue,agence \
    FROM vehicule JOIN Modele ON vehicule.modele = Modele.nom JOIN Categorie \
    ON Categorie.nom = Modele.Categorie WHERE Categorie.nom LIKE \
    '{}{}' ORDER BY numero_immat".format(categ_choisie,'%')
    if not BDD().execute(voiture_modele):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures recherchees ----")
    voitures.insert(0,("numero_immat","couleur","tarif","modele","typecarbu","loue","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
    
def Voiture_ajout():
    voiture_ajout()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()


def voiture_ajout():
    print("---- Vous etes au menu d'ajout de voiture a la BDD ----\n\n")
    plaque = Menu.input_plaque()

    couleur = input("Saisissez la couleur du vehicule : ")
     
    nbkm = Menu.input_int("Saisissez le nombre de km du vehicule : ")
    
    prix = round(Menu.input_float("Saisissez le prix journalier du vehicule : "),2)
    
    sql_modele = "SELECT * FROM Modele ORDER BY nom;"
    if not BDD().execute(sql_modele):
        return;
    modele = BDD().fetchall()
    print("---- Les differentes modeles ----")
    modele.insert(0,("nom","marque","nbPorte","categorie"))
    print(tabulate(modele,headers='firstrow',tablefmt='psql'))
        
    modele = input("Saisissez le modele du vehicule : ")
    modele_sql = "SELECT nom FROM Modele"
    if not BDD().execute(modele_sql):
        return;
    modeles = BDD().fetchall()
    modele_exists = False
    for i in modeles:
        if i[0].lower() == modele.lower():
            modele_exists = True
            break
    if not modele_exists:
        print("Modele inconnu, ajout a la BDD...")
        input("Appuyez sur une touche pour continuer...")
        categ_sql = "SELECT * FROM Categorie"
        if not BDD().execute(categ_sql):
            return;
        categ = BDD().fetchall()
        print("---- Categories disponibles ----")
        categ.insert(0,("categorie",))
        print(tabulate(categ,headers='firstrow',tablefmt='psql'))
        categ_modele = ""
        while (categ_modele,) not in categ:
            categ_modele = input("Saisissez la categorie pour le modele {} : ".format(modele))
        nb_porte = int(input("Saisissez le nombre de portes pour le modele {} : ".format(modele)))
        marque = input("Saisissez la marque pour le modele {} : ".format(modele))
        modele_sql_add = "INSERT INTO Modele (nom,marque,nbPortes,categorie) VALUES('{}','{}',{},'{}')".format(modele,marque,nb_porte,categ_modele) 
        if not BDD().execute(modele_sql_add):
            return;
        BDD().commit()
        print("---- Ajout modele reussi ----")
    
    carbu_sql = "SELECT * FROM TypeCarburant"
    if not BDD().execute(carbu_sql):
        return;
    carbus = BDD().fetchall()
    print("---- Carburants disponibles ----")
    carbus.insert(0,("carburant",))
    print(tabulate(carbus,headers='firstrow',tablefmt='psql'))
    type_carbu = ""
    while (type_carbu,) not in carbus:
        type_carbu = input("Saisissez le type de carburant pour le modele {} : ".format(modele))
    
    loue = False
    
    agence_sql = "SELECT * FROM Agence_location"
    if not BDD().execute(agence_sql):
        return;
    agences = BDD().fetchall()
    print("---- Agences disponibles ----")
    agences.insert(0,("adresse","nom"))
    print(tabulate(agences,headers='firstrow',tablefmt='psql'))
    agence_num = 0
    while agence_num < 1 or agence_num > len(agences)-1:
        agence_num = int(input("Saisissez le numero de l'agence (numero de ligne) pour ce vehicule : "))
    agence = agences[agence_num][0]
    
    voiture_ajout_sql = "INSERT INTO Vehicule (numero_immat,couleur,nbkm,tarif,modele,typeCarbu,loue,agence) VALUES \
('{}','{}',{},{},'{}','{}',{},'{}')".format(plaque,couleur,nbkm,prix,modele,type_carbu,loue,agence)
    if not BDD().execute(voiture_ajout_sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
    voiture_sql = "SELECT * FROM Vehicule WHERE numero_immat = '{}'".format(plaque)
    if not BDD().execute(voiture_sql):
        return;
    voiture = BDD().fetchall()
    voiture.insert(0,("numero_immat","couleur","tarif","modele","typecarbu","loue","agence"))
    print(tabulate(voiture,headers='firstrow',tablefmt='psql'))
    
    input("Appuyez sur une touche pour selectionner les options du vehicule.")
    Menu.clear()

    query_option = ("select nom from option_tab;")
    if not BDD().execute(query_option):
        return;
    options = [i[0] for i in BDD().fetchall()]
    nb_option = len(options)

    print("---- Options possibles ----")
    print('\n'.join("{}. {}".format(i+1, options[i]) for i in range(nb_option)))
    print("{}. Creer une option".format(nb_option+1))
    choix_option = {0}
    while False in (1<=choix<=nb_option+1 for choix in choix_option):
        choix_option = set((int(val) for val in
        input("Selectionnez une liste de numero d'option (separe par des espaces)").split() if val.isdigit()))


    if nb_option+1 in choix_option:
        print("---- Ajout nouvelle option vehicule ----")
        nvl_option = ""
        while(nvl_option != "Q"):
            if nvl_option :
                print("L'option a bien ete ajoute\n-----------------------------")
            nvl_option = input(
                "Donnez le nom de la nouvelle option (Q pour quitter): ")
            if nvl_option != "Q":
                query_creation_option =  "INSERT INTO option_tab VALUES ('{}')".\
                    format(nvl_option)
                if not BDD().execute(query_creation_option):
                    return;
                BDD().commit()

    query_add_option = "insert into option_vehicule(option_v, vehicule)\
        values {};".format(
            ','.join("('{}', '{}')".format(options[i-1], plaque)
            for i in choix_option if i!=nb_option+1)
            )
    if not BDD().execute(query_add_option):
        return;
    BDD().commit()


    
def Attente_controle():
    attente_controle()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def attente_controle():
    query_attent_controle = "select distinct v.numero_immat, v.couleur,\
    v.tarif, v.modele, v.typecarbu, v.agence\
    from controles c left outer join contratdelocation_pro c_pr on\
    c.contrat_loc_pro = c_pr.numero\
    left outer join contratdelocation_part c_pa on\
    c.contrat_loc_part = c_pa.numero\
    left outer join vehicule v on\
    c_pr.vehicule = v.numero_immat or c_pa.vehicule = v.numero_immat\
    where c.date_realisation is null;"
    if not BDD().execute(query_attent_controle):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures en attente de controle ----")
    voitures.insert(
        0, ("numero_immat","couleur","tarif","modele","typecarbu","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
    

    
def affiche_moy_paye():
    sql = "SELECT * FROM Moyen_paiement"
    if not BDD().execute(sql):
        return;
    moy = BDD().fetchall()
    print("---- Moyen de paiement ----")
    moy.insert(0,("nom",))
    print(tabulate(moy,headers='firstrow',tablefmt='psql'))