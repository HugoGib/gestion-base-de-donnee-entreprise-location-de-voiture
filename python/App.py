import sys
import subprocess
Error = True
while Error:
    try:
        from tabulate import tabulate
        Error = False
    except ImportError as e:
        print("Le package 'tabulate' est manquant et va etre installe")
        input("Appuyez sur une touche pour continuer...")
        subprocess.check_call([sys.executable,"-m","pip","install","tabulate"])

from Menu import Menu
from BDD import BDD
import sys
import subprocess


Error = True
while Error:
    try:
        from tabulate import tabulate
        Error = False
    except ImportError as e:
        print("Le package 'tabulate' est manquant et va etre installe")
        input("Appuyez sur une touche pour continuer...")
        subprocess.check_call([sys.executable,"-m","pip","install","tabulate"])

import Agence
import Voitures
import Personnel
import Client
import Contrats
import Factures


class MenuPrincipal(Menu):

    def __init__(self):
        print("---- Logiciel de gestion de la BDD de l'agence de location de voiture ----\n\n\n")

        if not Menu.choix_binaire("Votre BDD existe-t-elle ? O/N "):
            BDD().create_tables()

        if Menu.choix_binaire("Voulez-vous ajouter des donnees a votre BDD depuis un fichier sql ? O/N "):
            BDD().add_data()


        if not Menu.choix_binaire("Voulez-vous acceder au gestionnaire de la BDD ? O(oui)/N(quitte le programme) "):
            input("Le programme va se quitter appuyez sur n'importe quelle touche...")
            sys.exit()
        else:
            Menu.clear()
            self.menu_gestion()

    def menu_gestion(self):
        while True:
            print("---- Vous etes au menu de gestion ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Gestionnaire de voiture")
            print("2. Gestionnaire de client")
            print("3. Gestionnaire de location")
            print("4. Gestionnaire de la BDD")
            print("5. Gestion du personnel")
            print("6. Quitter le programme")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                self.gestion_voiture()
            elif choix == 2:
                Menu.clear()
                self.gestion_client()
            elif choix == 3:
                Menu.clear()
                self.gestion_location()
            elif choix == 4:
                Menu.clear()
                self.gestion_bdd()
            elif choix == 5:
                Menu.clear()
                self.gestion_personnel()
            elif choix == 6:
                Menu.clear()
                input("Arret de l'application appuyez sur n'importe quelle touche")
                sys.exit()
            else:
                print("Merci de choisir un numero valide")

    def gestion_voiture(self):
        while True:
            print("---- Vous etes au menu de gestion de voiture ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Voir le parc des voitures")
            print("2. Voir les voitures disponibles")
            print("3. Chercher une voiture")
            print("4. Voir les voitures louees")
            print("5. Voir les voitures en attente de controle")
            print("6. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Voitures.Parc_voiture()
            elif choix == 2:
                Menu.clear()
                Voitures.Voitures_dispo()
            elif choix == 3:
                Menu.clear()
                self.chercher_voiture()
            elif choix == 4:
                Menu.clear()
                Voitures.Voitures_louees()
            elif choix == 5:
                Menu.clear()
                Voitures.Attente_controle()
            elif choix == 6:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")

    def gestion_client(self):
        while True:
            print("---- Vous etes au menu de gestion des clients ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Gestion client particulier")
            print("2. Gestion client professionnel")
            print("3. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                self.gestion_client_particulier()
            elif choix == 2:
                Menu.clear()
                self.gestion_client_professionnel()
            elif choix == 3:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")

    def gestion_bdd(self):
        while True:
            print("\n\n---- Vous etes au menu de gestion de la BDD ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Voir l'etat de la connexion")
            print("2. Ajout de data depuis un .sql")
            print("3. Supprimer les tables de la BDD")
            print("4. Creer les tables")
            print("5. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            print("\n\n")
            if choix == 1:
                Menu.clear()
                BDD().etat_connexion()
            elif choix == 2:
                Menu.clear()
                BDD().add_data()
            elif choix == 3:
                Menu.clear()
                BDD().drop_tables()
            elif choix == 4:
                Menu.clear()
                BDD().create_tables()
            elif choix == 5:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")



    def gestion_personnel(self):
        while True:
            print("---- Vous etes au menu de gestion du personnel ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Ajouter une agence")
            print("2. Ajout societe d'entretien")
            print("3. Gestion des agents")
            print("4. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Agence.ajout_agence()
            elif choix == 2:
                Menu.clear()
                Personnel.Ajout_societe_entretien()
            elif choix == 3:
                Menu.clear()
                self.gestion_agents()
            elif choix == 4:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")

    def chercher_voiture(self):
        while True:
            print("---- Vous etes au menu de recherche de voiture ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Chercher une voiture par plaque")
            print("2. Chercher une voiture par modele")
            print("3. Chercher une voiture par categorie")
            print("4. Afficher options d'une voiture")
            print("5. Ajouter une voiture a la BDD")
            print("6. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Voitures.Voiture_plaque()
            elif choix == 2:
                Menu.clear()
                Voitures.Voiture_modele()
            elif choix == 3:
                Menu.clear()
                Voitures.Voiture_categorie()
            elif choix == 4:
                Menu.clear()
                Voitures.Voiture_option()
            elif choix == 5:
                Menu.clear()
                Voitures.Voiture_ajout()
            elif choix == 6:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")

    def gestion_client_particulier(self):
        while True:
            print("---- Vous etes au menu de gestion des clients particuliers ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Afficher la liste des clients particuliers")
            print("2. Chercher un client par nom")
            print("3. Chercher un client par numero de permis")
            print("4. Ajouter un client")
            print("5. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Client.Affiche_client_particulier()
            elif choix == 2:
                Menu.clear()
                Client.Cherche_part_nom()
            elif choix == 3:
                Menu.clear()
                Client.Cherche_part_permis()
            elif choix == 4:
                Menu.clear()
                Client.Ajout_client_part()
            elif choix == 5:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")

    def gestion_client_professionnel(self):
        while True:
            print("---- Vous etes au menu de gestion des clients professionnels ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Afficher la liste des clients professionnels")
            print("2. Chercher un client pro par nom")
            print("3. Afficher conducteur(s) d'un client pro")
            print("4. Ajouter un client pro")
            print("5. Ajouter un conducteur")
            print("6. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Client.Affiche_client_pro()
            elif choix == 2:
                Menu.clear()
                Client.Cherche_pro_nom()
            elif choix == 3:
                Menu.clear()
                Client.Affiche_conduct_pro()
            elif choix == 4:
                Menu.clear()
                Client.Ajout_client_pro()
            elif choix == 5:
                Menu.clear()
                Client.Ajout_conducteur_pro()
            elif choix == 6:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")
    
    def gestion_agents(self):
        while True:
            print("---- Vous etes au menu de gestion des agents ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Afficher tous les agents")
            print("2. Ajouter un agent commercial")
            print("3. Ajouter un agent technique")
            print("4. Quitter ce menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Personnel.Afficher_agents()
            elif choix == 2:
                Menu.clear()
                Personnel.Ajout_agent_commercial()
            elif choix == 3:
                Menu.clear()
                Personnel.Ajout_agent_technique()
            elif choix == 4:
                Menu.clear()
                break
            else:
                print("Merci de choisir un numero valide")


    def gestion_location(self):
        while True:
            print("---- Vous etes au menu de gestion des locations ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Gestionnaire de contrats")
            print("2. Gestionnaire de factures")
            print("3. Quitter le menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                self.gestion_contrats()
            elif choix == 2:
                Menu.clear()
                self.gestion_factures()
            elif choix == 3:
                Menu.clear()
                break

    def gestion_contrats(self):
        while True:
            print("---- Vous etes au menu de gestion des locations ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Afficher tous les contrats d'un vehicule")
            print("2. Afficher tous les contrats lies a une categorie de vehicule")
            print("3. Afficher tous les contrats lies a un modele de vehicule")
            print("4. Bilan financier client")
            print("5. Afficher tous les contrats non valides")
            print("6. Valider un contrat pour un client particulier")
            print("7. Valider un contrat pour un client professionnel")
            print("8. Ajouter un contrat pour un client particulier")
            print("9. Ajouter un contrat pour un client professionnel")
            print("10. Quitter le menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Contrats.Afficher_contrat_vehicule()
            elif choix == 2:
                Menu.clear()
                Contrats.Afficher_contrat_categorie_vehicule()
            elif choix == 3:
                Menu.clear()
                Contrats.Afficher_contrat_modele_vehicule()
            elif choix == 4:
                Menu.clear()
                self.gestion_Bilan_client()
            elif choix == 5:
                Menu.clear()
                Contrats.Contrats_non_valides()
            elif choix == 6:
                Menu.clear()
                Contrats.Valider_contrat_part()
            elif choix == 7:
                Menu.clear()
                Contrats.Valider_contrat_pro()
            elif choix == 8:
                Menu.clear()
                Contrats.Ajouter_contrat_part()
            elif choix == 9:
                Menu.clear()
                Contrats.Ajouter_contrat_pro()
            elif choix == 10:
                Menu.clear()
                break

    def gestion_factures(self):
        while True:
            print("---- Vous etes au menu de gestion des factures ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Bilan factures client")
            print("2. Afficher toutes les factures non payees")
            print("3. Valider une facture pour un client particulier")
            print("4. Valider une facture pour un client professionnel")
            print("5. Ajouter une facture pour un client particulier")
            print("6. Ajouter une facture pour un client professionnel")
            print("7. Quitter le menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                self.gestion_Bilan_facture_client()
            elif choix == 2:
                Menu.clear()
                Factures.Factures_non_payees()
            elif choix == 3:
                Menu.clear()
                Factures.Payer_facture_part()
            elif choix == 4:
                Menu.clear()
                Factures.Payer_facture_pro()
            elif choix == 5:
                Menu.clear()
                Factures.Ajouter_facture_part()
            elif choix == 6:
                Menu.clear()
                Factures.Ajouter_facture_pro()
            elif choix == 7:
                Menu.clear()
                break

    def gestion_Bilan_client(self):
        while True:
            print("---- Vous etes au menu de gestion des locations ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Voir le Bilan des contrats client particulier")
            print("2. Voir le Bilan des contrats client professionnel")
            print("3. Quitter le menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Client.Bilan_client_part()
            elif choix == 2:
                Menu.clear()
                Client.Bilan_client_pro()
            elif choix == 3:
                Menu.clear()
                break

    def gestion_Bilan_facture_client(self):
        while True:
            print("---- Vous etes au menu de gestion des locations ----\n\n")
            print("Que souhaitez-vous faire ?")
            print("1. Voir le Bilan des factures client particulier")
            print("2. Voir le Bilan des factures client professionnel")
            print("3. Quitter le menu")
            choix = Menu.input_int("Choix ?  ")
            if choix == 1:
                Menu.clear()
                Client.Bilan_facture_client_part()
            elif choix == 2:
                Menu.clear()
                Client.Bilan_facture_client_pro()
            elif choix == 3:
                Menu.clear()
                break



def main():  
	MenuPrincipal()
	BDD().close_connexion()

if __name__ == '__main__':
    main()