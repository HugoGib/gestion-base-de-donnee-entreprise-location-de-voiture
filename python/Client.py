from BDD import BDD
from tabulate import tabulate
from Menu import Menu

def Affiche_client_particulier():
    affiche_client_particulier()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def affiche_client_particulier():
    client_part_sql = "SELECT * FROM client_particulier ORDER BY nom"
    if not BDD().execute(client_part_sql):
        return;
    clients = BDD().fetchall()
    print("---- Clients particulier dans la BDD ----")
    clients.insert(0,("numero_permis","nom","prenom","adresse","numero tel","ddn"))
    print(tabulate(clients,headers='firstrow',tablefmt='psql'))
    

def Cherche_part_nom():
    cherche_part_nom()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def cherche_part_nom():
    nom = input("Saisir le nom du client a chercher : ")
    if nom == '':
        return
    nom = nom[0].upper() + nom[1:]
    nom_sql = "SELECT * FROM client_particulier WHERE nom \
    LIKE '{}{}' ORDER BY prenom".format(nom,'%')
    if not BDD().execute(nom_sql):
        return;
    noms = BDD().fetchall()
    print("---- Clients recherches ----")
    noms.insert(0,("numero_permis","nom","prenom","adresse","numero tel","ddn"))
    print(tabulate(noms,headers='firstrow',tablefmt='psql'))
    

def Cherche_part_permis():
    cherche_part_permis()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def cherche_part_permis():
    permis = input("Saisir le numero de permis du client a chercher : ").upper()
    permis_sql = "SELECT * FROM client_particulier WHERE numero_permis \
    LIKE '{}{}' ORDER BY nom".format(permis,'%')
    if not BDD().execute(permis_sql):
        return;
    noms = BDD().fetchall()
    print("---- Clients recherches ----")
    noms.insert(0,("numero_permis","nom","prenom","adresse","numero tel","ddn"))
    print(tabulate(noms,headers='firstrow',tablefmt='psql'))
    

def Ajout_client_part():
    ajout_client_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajout_client_part():
    print("---- Vous etes au menu d'ajout de client particulier a la BDD ----\n\n")
    nom = input("Saisissez le nom du client particulier a ajouter : ")
    prenom = input("Saisissez le prenom du client particulier a ajouter : ")
    numero_permis = Menu.input_permis()
    
    adresse = input("Saisissez l'adresse du client {} {} : ".format(nom,prenom))
    numero_tel = Menu.input_numero_tel()
    ddn = Menu.input_ddn()
    
    client_ajout_sql = "INSERT INTO Client_particulier (numero_permis,nom,prenom,adresse,numero_tel,ddn) \
    VALUES ('{}','{}','{}','{}',{},'{}')".format(numero_permis,nom,prenom,adresse,numero_tel,ddn)
    
    if not BDD().execute(client_ajout_sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
    client_sql = "SELECT * FROM Client_particulier WHERE numero_permis = '{}'".format(numero_permis)
    if not BDD().execute(client_sql):
        return;
    client = BDD().fetchall()
    client.insert(0,("numero_permis","nom","prenom","adresse","numero_tel","ddn"))
    print(tabulate(client,headers='firstrow',tablefmt='psql'))
    
    

def Affiche_client_pro():
    affiche_client_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()


def affiche_client_pro():
    client_pro_sql = "SELECT * FROM client_pro ORDER BY nom"
    if not BDD().execute(client_pro_sql):
        return;
    clients = BDD().fetchall()
    print("---- Clients professionnels dans la BDD ----")
    clients.insert(0,("nom","info"))
    print(tabulate(clients,headers='firstrow',tablefmt='psql'))
    
def Cherche_pro_nom():
    cherche_pro_nom()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def cherche_pro_nom():
    nom = input("Saisir le nom du client a chercher : ")
    if nom == '':
        return
    nom = nom[0].upper() + nom[1:]
    nom_sql = "SELECT * FROM client_pro WHERE nom \
    LIKE '{}{}'".format(nom,'%')
    if not BDD().execute(nom_sql):
        return;
    noms = BDD().fetchall()
    print("---- Clients recherches ----")
    noms.insert(0,("nom","info"))
    print(tabulate(noms,headers='firstrow',tablefmt='psql'))
    

def Affiche_conduct_pro():
    affiche_conduct_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def affiche_conduct_pro(nom = ''):
    if nom == '':
        nom = input("Saisir le nom du client pro du conducteur : ")
        nom = nom[0].upper() + nom[1:]
    conduct_sql = "SELECT * FROM conducteur WHERE entreprise \
    LIKE '{}{}'".format(nom,'%')
    
    if not BDD().execute(conduct_sql):
        return;
    noms = BDD().fetchall()
    print("---- Conducteurs recherches ----")
    noms.insert(0,("numero_permis","nom","prenom","ddn","entreprise"))
    print(tabulate(noms,headers='firstrow',tablefmt='psql'))
    
def Ajout_client_pro():
    ajout_client_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajout_client_pro():
    print("---- Vous etes au menu d'ajout de client professionnel a la BDD ----\n\n")
    nom = input("Saisissez le nom du client pro a ajouter : ")
    nom = nom[0].upper() + nom[1:]
    info = input("Saisissez des info sur le client pro a ajouter : ")
    
    client_ajout_sql = "INSERT INTO Client_pro (nom,info_ets) \
    VALUES ('{}','{}')".format(nom,info)
    
    if not BDD().execute(client_ajout_sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
    client_sql = "SELECT * FROM Client_pro WHERE nom = '{}'".format(nom)
    if not BDD().execute(client_sql):
        return;
    client = BDD().fetchall()
    client.insert(0,("nom","info client"))
    print(tabulate(client,headers='firstrow',tablefmt='psql'))
    
def Ajout_conducteur_pro():
    ajout_conducteur_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajout_conducteur_pro():
    print("---- Vous etes au menu d'ajout de conducteur pro a la BDD ----\n\n")
    
    
    client_pro_sql = "SELECT * FROM client_pro ORDER BY nom"
    if not BDD().execute(client_pro_sql):
        return;
    clients = BDD().fetchall()
    print("---- Clients professionnels dans la BDD ----")
    clients.insert(0,("nom","info"))
    print(tabulate(clients,headers='firstrow',tablefmt='psql'))
    
    entreprise_num = 0
    while entreprise_num < 1 or entreprise_num > len(clients)-1:
        entreprise_num = int(input("Saisissez la ligne correspondant a l'entreprise du conducteur : "))
    entreprise = clients[entreprise_num][0]
    
    nom = input("Saisissez le nom du client particulier a ajouter : ")
    prenom = input("Saisissez le prenom du client particulier a ajouter : ")
    numero_permis = Menu.input_permis()
    
    ddn = Menu.input_ddn()
    
    
    
    conduct_ajout_sql = "INSERT INTO Conducteur (numero_permis,nom,prenom,ddn,entreprise) \
    VALUES ('{}','{}','{}','{}','{}')".format(numero_permis,nom,prenom,ddn,entreprise)
    
    if not BDD().execute(conduct_ajout_sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
    client_sql = "SELECT * FROM Conducteur WHERE numero_permis = '{}'".format(numero_permis)
    if not BDD().execute(client_sql):
        return;
    client = BDD().fetchall()
    client.insert(0,("numero_permis","nom","prenom","ddn","entreprise"))
    print(tabulate(client,headers='firstrow',tablefmt='psql'))

def Bilan_client_part():
    bilan_client_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def bilan_client_part():
    affiche_client_particulier()
    client_permis = Menu.input_permis()
    bilan_sql = "SELECT * FROM ContratDeLocation_part WHERE signe_par = '"+client_permis+"' ORDER BY numero"
    if not BDD().execute(bilan_sql):
        return;
    bilan = BDD().fetchall()
    print("---- Bilan du client ----")
    bilan.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(bilan,headers='firstrow',tablefmt='psql'))


def Bilan_client_pro():
    bilan_client_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def bilan_client_pro():
    affiche_client_pro()
    client_nom = input("Saisissez le nom du client professionnel : ")
    bilan_sql = "SELECT * FROM ContratDeLocation_pro WHERE signe_par = '"+client_nom+"' ORDER BY numero"
    if not BDD().execute(bilan_sql):
        return;
    bilan = BDD().fetchall()
    print("---- Bilan du client ----")
    bilan.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture","conducteur"))
    print(tabulate(bilan,headers='firstrow',tablefmt='psql'))


def Bilan_facture_client_part():
    bilan_facture_client_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def bilan_facture_client_part():
    affiche_client_particulier()
    client_permis = Menu.input_permis()
    bilan_sql = "SELECT * FROM Facture_particulier WHERE paye_par = '"+client_permis+"' ORDER BY numero"
    if not BDD().execute(bilan_sql):
        return;
    bilan = BDD().fetchall()
    print("---- Bilan du client ----")
    bilan.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(bilan,headers='firstrow',tablefmt='psql'))

def Bilan_facture_client_pro():
    bilan_facture_client_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def bilan_facture_client_pro():
    affiche_client_pro()
    client_nom = input("Saisissez le nom du client professionnel : ")
    bilan_sql = "SELECT * FROM Facture_pro WHERE paye_par = '"+client_nom+"' ORDER BY numero"
    if not BDD().execute(bilan_sql):
        return;
    bilan = BDD().fetchall()
    print("---- Bilan du client ----")
    bilan.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(bilan,headers='firstrow',tablefmt='psql'))