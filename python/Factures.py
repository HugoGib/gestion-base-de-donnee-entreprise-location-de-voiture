from Menu import Menu
from BDD import BDD
from tabulate import tabulate
import Voitures
import Client
import Personnel

def Factures_non_payees():
    factures_non_payees()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def factures_non_payees():
    print("---- Vous etes au menu d'affichage des factures non payees ----\n\n")
    sql = "SELECT * FROM Facture_particulier WHERE payee = FALSE\
    UNION SELECT * FROM Facture_pro WHERE payee = FALSE"
    if not BDD().execute(sql):
        return;
    factures = BDD().fetchall()
    print("---- Factures non payees ----")
    factures.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(factures,headers='firstrow',tablefmt='psql'))

def factures_non_payees_part():
    print("---- Vous etes au menu d'affichage des factures non validees ----\n\n")
    sql = "SELECT * FROM Facture_particulier WHERE payee = FALSE"
    if not BDD().execute(sql):
        return;
    factures = BDD().fetchall()
    print("---- Factures non payees ----")
    factures.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(factures,headers='firstrow',tablefmt='psql'))
	
def factures_non_payees_pro():
    print("---- Vous etes au menu d'affichage des factures non validees ----\n\n")
    sql = "SELECT * FROM Facture_pro WHERE payee = FALSE"
    if not BDD().execute(sql):
        return;
    factures = BDD().fetchall()
    print("---- Factures non payees ----")
    factures.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(factures,headers='firstrow',tablefmt='psql'))
    
def Payer_facture_part():
    payer_facture_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def payer_facture_part():
    print("---- Vous etes au menu pour confirmer le paiement d une facture ----\n\n")
    factures_non_payees_part()
    to_continue = Menu.choix_binaire("Si pas de facture dispo tapez N sinon o")
    if not to_continue:
        return
    numero_facture = Menu.input_int("Quel est le numero de la facute a valider ? ")
    sql = "SELECT * FROM Agent_commercial;"
    if not BDD().execute(sql):
        return;
    agent_C = BDD().fetchall()
    print("---- Agents commerciaux ----")
    agent_C.insert(0,("id","nom","prenom","agence"))
    print(tabulate(agent_C,headers='firstrow',tablefmt='psql'))
    id_agent_com = Menu.input_int("Saisissez ID de l agent commercial qui valide le contrat : ")
    date_paiement = Menu.input_date("Saisissez la date de paiement : (YYYY-MM-JJ) ")
    Voitures.affiche_moy_paye()
    moy_paye = input("Saisir le moyen de paiement : ")
    sql = "UPDATE Facture_particulier SET payee = TRUE, date_paiement = '{}' , paiement = '{}', agent_com = {} WHERE numero = '{}'".format(date_paiement,moy_paye,id_agent_com,numero_facture)
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Paiement reussi ----")
    

def Payer_facture_pro():
    payer_facture_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def payer_facture_pro():
    print("---- Vous etes au menu pour confirmer le paiement d une facture ----\n\n")
    factures_non_payees_pro()
    to_continue = Menu.choix_binaire("Si pas de facture dispo tapez N sinon o")
    if not to_continue:
        return
    numero_facture = Menu.input_int("Quel est le numero de la facute a valider ? ")
    sql = "SELECT * FROM Agent_commercial;"
    if not BDD().execute(sql):
        return;
    agent_C = BDD().fetchall()
    print("---- Agents commerciaux ----")
    agent_C.insert(0,("id","nom","prenom","agence"))
    print(tabulate(agent_C,headers='firstrow',tablefmt='psql'))
    id_agent_com = Menu.input_int("Saisissez ID de l agent commercial qui valide le contrat : ")
    date_paiement = Menu.input_date("Saisissez la date de paiement : (YYYY-MM-JJ) ")
    Voitures.affiche_moy_paye()
    moy_paye = input("Saisir le moyen de paiement : ")
    sql = "UPDATE Facture_pro SET payee = TRUE, date_paiement = '{}' , paiement = '{}', agent_com = {} WHERE numero = '{}'".format(date_paiement,moy_paye,id_agent_com,numero_facture)
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Paiement reussi ----")


def Ajouter_facture_pro():
    ajouter_facture_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajouter_facture_pro():
    print("---- Vous etes au menu d ajout d une facture professionel a la BDD ----\n\n")
    sql = "SELECT Max(numero) FROM Facture_pro;"
    if not BDD().execute(sql):
        return;
    numero = BDD().fetchall()[0][0] + 1
    montant = Menu.input_float("Saisissez le montant de la facture  : ")
    payee = str("FALSE")
    Client.affiche_client_pro()
    paye_par = input("Entrez le nom du client professionel lie a ce contrat : ")
    Personnel.afficher_agents_com()
    agent_com = Menu.input_int("Entrez l id de l agent commercial lie au contrat : ")
    paiement = "NULL"
    
    sql = "INSERT INTO Facture_ppro (numero,montant,payee,paiement,paye_par,agent_com) \
    VALUES ({},{},{},{},'{}','{}')".format(numero, montant, payee, paiement, paye_par, agent_com)
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")

def Ajouter_facture_part():
    ajouter_facture_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajouter_facture_part():
    print("---- Vous etes au menu d'ajout de facture pour un client particulier a la BDD ----\n\n")
    sql = "SELECT Max(numero) FROM Facture_particulier;"
    if not BDD().execute(sql):
        return;
    numero = BDD().fetchall()[0][0] + 1
    montant = Menu.input_float("Saisissez le montant de la facture  : ")
    payee = str("FALSE")
    
    print("Entrez le permis du client personel lie a cette facture")
    Client.affiche_client_particulier()
    paye_par = Menu.input_permis()
    Personnel.afficher_agents_com()
    agent_com = Menu.input_int("Entrez l id de l agent commercial lie au contrat : ")
    paiement = "NULL"
    
    sql = "INSERT INTO Facture_particulier (numero,montant,payee,paiement,paye_par,agent_com)\
    VALUES ({},{},{},{},'{}','{}')".format(numero, montant, payee, paiement, paye_par, agent_com)
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
def Affiche_facture_pro(ets):
    affiche_facture_pro(ets)
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def affiche_facture_pro(ets):
    sql = "SELECT * FROM Facture_pro WHERE paye_par = '%s'" %(ets)
    if not BDD().execute(sql):
        return;
    factures = BDD().fetchall()
    print("---- Factures pour le client : %s ----" %(ets))
    factures.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(factures,headers='firstrow',tablefmt='psql'))
    
def Affiche_facture_part(num_perm):
    affiche_facture_part(num_perm)
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def affiche_facture_part(num_perm):
    sql = "SELECT * FROM Facture_particulier WHERE paye_par = '%s'" %(num_perm)
    if not BDD().execute(sql):
        return;
    factures = BDD().fetchall()
    print("---- Factures pour le client : %s ----" %(num_perm))
    factures.insert(0,("numero","montant","payee","date paiement","mode paiement","agent_com","payee par"))
    print(tabulate(factures,headers='firstrow',tablefmt='psql'))
    
#SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,PrixDepassementKm,signe_par, agent_com,vehicule,facture FROM ContratDeLocation_part JOIN Vehicule ON ContratDeLocation_part.vehicule = Vehicule.numero_immat JOIN Modele ON Vehicule.modele = Modele.nom JOIN Categorie ON Categorie.nom = Modele.categore  WHERE Categorie.nom = 'citadine' UNION SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,PrixDepassementKm,signe_par, agent_com,vehicule,facture FROM ContratDeLocation_pro JOIN Vehicule ON ContratDeLocation_pro.vehicule = Vehicule.numero_immat JOIN Modele ON Vehicule.modele = Modele.nom JOIN Categorie ON Categorie.nom = Modele.categore WHERE categorie.nom = 'citadine';