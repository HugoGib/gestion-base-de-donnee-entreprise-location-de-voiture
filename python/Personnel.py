from BDD import BDD
from tabulate import tabulate
from Menu import Menu

def Ajout_societe_entretien():
    ajout_societe_entretien()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajout_societe_entretien():
        print("---- Vous etes au menu d'ajout de societe d'entretien a la BDD ----\n\n")
        nom = input("Saisissez le nom de la societe a ajouter : ")
        nom = nom[0].upper() + nom[1:]
        
        societe_ajout_sql = "INSERT INTO Societe_entretien (nom) \
        VALUES ('{}')".format(nom)
        
        if not BDD().execute(societe_ajout_sql):
            return;
        BDD().commit()
        print("---- Ajout reussi ----")
        
        agence_sql = "SELECT * FROM Societe_entretien WHERE nom = '{}'".format(nom)
        if not BDD().execute(agence_sql):
            return;
        agence = BDD().fetchall()
        agence.insert(0,("nom",))
        print(tabulate(agence,headers='firstrow',tablefmt='psql'))
        
 
def Afficher_agents():
    afficher_agents()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def afficher_agents():
    print("----- Agents dans la BDD -----")
    afficher_agents_com()
    afficher_agents_tech()
#    agents_sql = "SELECT * FROM Agent_technique UNION SELECT * FROM Agent_commercial ORDER BY nom"
#    if not BDD().execute(agents_sql):
#        return;
#    agents= BDD().fetchall()
#    print("---- Agents dans la BDD ----")
#    agents.insert(0,("id","nom","prenom","agence"))
#    print(tabulate(agents,headers='firstrow',tablefmt='psql'))
    
def Afficher_agents_com():
    afficher_agents_com()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()
    
def afficher_agents_com():
    agents_sql = "SELECT * FROM Agent_commercial ORDER BY id"
    if not BDD().execute(agents_sql):
        return;
    agents= BDD().fetchall()
    print("---- Agents commerciaux ----")
    agents.insert(0,("id","nom","prenom","agence"))
    print(tabulate(agents,headers='firstrow',tablefmt='psql'))

def Afficher_agents_tech():
    afficher_agents_tech()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()
    
def afficher_agents_tech():
    agents_sql = "SELECT * FROM Agent_technique ORDER BY id"
    if not BDD().execute(agents_sql):
        return;
    agents= BDD().fetchall()
    print("---- Agents commerciaux ----")
    agents.insert(0,("id","nom","prenom","agence"))
    print(tabulate(agents,headers='firstrow',tablefmt='psql'))
    
def Ajout_agent_commercial():
    ajout_agent_commercial()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajout_agent_commercial():
    print("---- Vous etes au menu d'ajout d'agent commecial a la BDD ----\n\n")
    
    new_id_sql = "SELECT MAX(id) FROM Agent_commercial"
    if not BDD().execute(new_id_sql):
        return;
    new_id = int(BDD().fetchone()[0])+1
    
    client_pro_sql = "SELECT * FROM Agence_location ORDER BY nom"
    if not BDD().execute(client_pro_sql):
        return;
    clients = BDD().fetchall()
    print("---- Agence location dans la BDD ----")
    clients.insert(0,("adresse","nom"))
    print(tabulate(clients,headers='firstrow',tablefmt='psql'))
    
    entreprise_num = 0
    while entreprise_num < 1 or entreprise_num > len(clients)-1:
        entreprise_num = int(input("Saisissez la ligne correspondant a l'agence de l'agent : "))
    agence = clients[entreprise_num][0]
    
    nom = input("Saisissez le nom de l'agent a ajouter : ")
    prenom = input("Saisissez le prenom de l'agent a ajouter : ")
    
    
    conduct_ajout_sql = "INSERT INTO Agent_commercial (id,nom,prenom,agence) \
    VALUES ({},'{}','{}','{}')".format(new_id,nom,prenom,agence)
    
    if not BDD().execute(conduct_ajout_sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
    client_sql = "SELECT * FROM Agent_commercial WHERE id = {}".format(new_id)
    if not BDD().execute(client_sql):
        return;
    client = BDD().fetchall()
    client.insert(0,("id","nom","prenom","agence"))
    print(tabulate(client,headers='firstrow',tablefmt='psql'))
    
def Ajout_agent_technique():
    ajout_agent_technique()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()    

def ajout_agent_technique():
    print("---- Vous etes au menu d'ajout d'agent commecial a la BDD ----\n\n")
    
    new_id_sql = "SELECT MAX(id) FROM Agent_technique"
    if not BDD().execute(new_id_sql):
        return;
    
    new_id = int(BDD().fetchone()[0])+1
    
    client_pro_sql = "SELECT * FROM Agence_location ORDER BY nom"
    if not BDD().execute(client_pro_sql):
        return;
    clients = BDD().fetchall()
    print("---- Agence location dans la BDD ----")
    clients.insert(0,("adresse","nom"))
    print(tabulate(clients,headers='firstrow',tablefmt='psql'))
    
    entreprise_num = 0
    while entreprise_num < 1 or entreprise_num > len(clients)-1:
        entreprise_num = int(input("Saisissez la ligne correspondant a l'agence de l'agent : "))
    agence = clients[entreprise_num][0]
    
    nom = input("Saisissez le nom de l'agent a ajouter : ")
    prenom = input("Saisissez le prenom de l'agent a ajouter : ")
    
    
    conduct_ajout_sql = "INSERT INTO Agent_technique (id,nom,prenom,agence) \
    VALUES ({},'{}','{}','{}')".format(new_id,nom,prenom,agence)
    
    if not BDD().execute(conduct_ajout_sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
    
    client_sql = "SELECT * FROM Agent_technique WHERE id = {}".format(new_id)
    if not BDD().execute(client_sql):
        return;
    client = BDD().fetchall()
    client.insert(0,("id","nom","prenom","agence"))
    print(tabulate(client,headers='firstrow',tablefmt='psql'))
    
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()