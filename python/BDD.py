from MenuConnexion import MenuConnexion
import psycopg2 as p
from Menu import Menu

class BDD:
    """Objet singleton qui gere la base de donnee."""
    
    _instance = None

    def __new__(cls):
        if cls._instance is None: 
            cls._instance = super(BDD, cls).__new__(cls)
            cls._instance.menu_connexion = MenuConnexion()
            cls._instance.connexion = cls._instance.menu_connexion.connexion
            cls._instance.curseur = cls._instance.connexion.cursor()
            return cls._instance
        else:
            return cls._instance
        
    def close_connexion(self):
        if BDD._instance != None:
            BDD._instance.connexion.close()

    def etat_connexion(self):
        print("---- Voici l'etat de la connexion ----\n")

        print("host :",self.menu_connexion.host)
        print("dbname :",self.menu_connexion.dbname)
        print("username :",self.menu_connexion.username)
        print("password :",'*'*len(self.menu_connexion.password),"\n")

        input("Appuyez sur une touche pour revenir au menu precedent")
        Menu.clear()

    def execute(self,instruction):
        """Return True si pas d'erreur"""
        try:
            self.curseur.execute(instruction)
            return True
        except Exception as e:
            print("\n/!\ Erreur /!\ \nMessage systeme : ",e)
            input("Appuyez sur une touche pour revenir au menu precedent")
            self.connexion.close()
            BDD._instance = None
        return False

    def fetchall(self):
        return self.curseur.fetchall()

    def fetchone(self):
        return self.curseur.fetchone()

    def commit(self):
        self.connexion.commit()

    def create_tables(self):
        path = input("Saisissez le chemin de votre fichier source (taper NoData pour quitter) :")
        if path == "NoData":
            print("Ajout des tables abandonne")
            input("Appuyez sur une touche pour revenir au menu precedent...")
            Menu.clear()
            return;

        with open(path,'r') as create_tables:
            if not self.execute(create_tables.read()):  
                self.create_tables()
                return;
            self.commit()
            print("---- Creation des tables reussie ----\n\n")
            input("Appuyez sur une touche pour revenir au menu precedent...")
            Menu.clear()

    def add_data(self):
        path = input("Saisissez le chemin de votre fichier source (taper NoData pour quitter) : ")
        if path == "NoData":
            print("Ajout des donnees abandonne")
            input("Appuyez sur une touche pour revenir au menu precedent...")
            Menu.clear()
            return;
        with open(path,'r') as f:
            if not self.execute(f.read()):
                self.add_data()
                return;  
            self.connexion.commit()
            print("---- Ajout des donnees reussi ----\n\n")
            input("Appuyez sur une touche pour revenir au menu precedent...")
            Menu.clear()

    def drop_tables(self):
        tables_sql = "SELECT * FROM information_schema.tables WHERE table_schema = 'public'"
        self.execute(tables_sql)
        tables = self.fetchall()
        to_drop=''
        for table in tables:
            to_drop += table[2] + ','
        to_drop = to_drop[:-1]
        print("Tables dans la BDD :", to_drop)
        if Menu.choix_binaire("Souhaitez-vous vraiment supprimer les tables ? O/N "):
            to_drop_sql = "DROP TABLE IF EXISTS %s CASCADE;" %(to_drop)
            self.execute(to_drop_sql)
            self.commit()
            print("---- Suppression des tables reussi ----")
            print("---- Veuillez recreer les tables avant de reutiliser le logiciel ----")
            print("---- Nous vous conseillons de redemarrer le logiciel ----")
            input("Appuyez sur une touche pour revenir au menu precedent...")
            Menu.clear()

def main():
    BDD().create_tables()

if __name__ == '__main__':
    main()

