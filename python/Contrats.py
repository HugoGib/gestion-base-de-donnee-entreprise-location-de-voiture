from Menu import Menu
from BDD import BDD
from tabulate import tabulate
import Voitures
import Client
import Personnel
import Factures

def Afficher_contrat_vehicule():
    afficher_contrat_vehicule()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def afficher_contrat_vehicule():
    print("---- Vous etes au menu d'affichage des contrats lies a un vehicule ----\n\n")
    voitures_sql = "SELECT * FROM vehicule ORDER BY numero_immat"
    if not BDD().execute(voitures_sql):
        return;
    voitures = BDD().fetchall()
    print("---- Voitures dans le parc de location ----")
    voitures.insert(0,("numero_immat","couleur","nbkm","tarif","modele","typecarbu","loue","agence"))
    print(tabulate(voitures,headers='firstrow',tablefmt='psql'))
    plaque = Menu.input_plaque()
    sql = "SELECT * FROM ContratDeLocation_part WHERE vehicule = '"+plaque+"' UNION \
    SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,PrixDepassementKm,signe_par,\
    agent_com,vehicule,facture FROM ContratDeLocation_pro WHERE vehicule = '"+plaque+" ORDER BY numero';"
    if not BDD().execute(sql):
        return;
    contrats = BDD().fetchall()
    print("---- Contrats du vehicule ----")
    contrats.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(contrats,headers='firstrow',tablefmt='psql'))
    
def Afficher_contrat_categorie_vehicule():
    afficher_contrat_categorie_vehicule()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def afficher_contrat_categorie_vehicule():
    print("---- Vous etes au menu d'affichage des contrats lies a une categorie de vehicule ----\n\n")
    sql_categorie = "SELECT * FROM Categorie ORDER BY nom;"
    if not BDD().execute(sql_categorie):
        return;
    categorie = BDD().fetchall()
    print("---- Les differentes categories ----")
    categorie.insert(0,("nom",))
    print(tabulate(categorie,headers='firstrow',tablefmt='psql'))
    categorie_nom = input("Saisissez le nom de la categorie du vehicules : ")

    sql = "SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,PrixDepassementKm,signe_par,\
    agent_com,vehicule,facture FROM ContratDeLocation_part JOIN Vehicule ON ContratDeLocation_part.vehicule = Vehicule.numero_immat \
    JOIN Modele ON Vehicule.modele = Modele.nom JOIN Categorie ON Categorie.nom = modele.categorie  WHERE\
    Categorie.nom = '"+categorie_nom+"' UNION SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,\
    PrixDepassementKm,signe_par, agent_com,vehicule,facture FROM ContratDeLocation_pro JOIN Vehicule ON \
    ContratDeLocation_pro.vehicule = Vehicule.numero_immat JOIN Modele ON Vehicule.modele = Modele.nom \
    JOIN Categorie ON Categorie.nom = modele.categorie WHERE categorie.nom ='"+categorie_nom+" ORDER BY numero';"

    if not BDD().execute(sql):
        return;
    contrats = BDD().fetchall()
    print("---- Contrats de la categorie de vehicules ----")
    contrats.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(contrats,headers='firstrow',tablefmt='psql'))
    
def Afficher_contrat_modele_vehicule():
    afficher_contrat_modele_vehicule()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def afficher_contrat_modele_vehicule():
    print("---- Vous etes au menu d'affichage des contrats lies a un modele de vehicule ----\n\n")
    sql_modele = "SELECT * FROM Modele ORDER BY nom;"
    if not BDD().execute(sql_modele):
        return;
    modele = BDD().fetchall()
    print("---- Les differentes modeles ----")
    modele.insert(0,("nom","marque","nbPorte","categorie"))
    print(tabulate(modele,headers='firstrow',tablefmt='psql'))
    modele_nom = input("Saisissez le nom du modele de vehicules : ")

    sql = "SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,PrixDepassementKm,signe_par,\
    agent_com,vehicule,facture FROM ContratDeLocation_part JOIN Vehicule ON ContratDeLocation_part.vehicule = Vehicule.numero_immat \
    JOIN Modele ON Vehicule.modele = Modele.nom WHERE Modele.nom = '"+modele_nom+"' UNION SELECT numero,\
    nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,PrixDepassementKm,signe_par, agent_com,vehicule,\
    facture FROM ContratDeLocation_pro JOIN Vehicule ON ContratDeLocation_pro.vehicule = Vehicule.numero_immat \
    JOIN Modele ON Vehicule.modele = Modele.nom WHERE Modele.nom ='"+modele_nom+" ORDER BY numero';"

    if not BDD().execute(sql):
        return;
    contrats = BDD().fetchall()
    print("---- Contrats lies au modele de vehicules ----")
    contrats.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(contrats,headers='firstrow',tablefmt='psql'))
    
def Contrats_non_valides():
    contrats_non_valides()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def contrats_non_valides():
    print("---- Vous etes au menu d'affichage des contrats non valides ----\n\n")
    sql = "SELECT * FROM ContratDeLocation_part WHERE validation = FALSE\
    UNION SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour, PrixDepassementKm,signe_par,\
    agent_com,vehicule,facture FROM ContratDeLocation_pro WHERE validation = FALSE ORDER BY numero"
    if not BDD().execute(sql):
        return;
    contrats = BDD().fetchall()
    print("---- Contrats non valides ----")
    contrats.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(contrats,headers='firstrow',tablefmt='psql'))

def Contrats_non_valides_part():
    contrats_non_valides_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def contrats_non_valides_part():
    print("---- Vous etes au menu d'affichage des contrats non valides ----\n\n")
    sql = "SELECT * FROM ContratDeLocation_part WHERE validation = FALSE ORDER BY numero"
    if not BDD().execute(sql):
        return;
    contrats = BDD().fetchall()
    print("---- Contrats non valides ----")
    contrats.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(contrats,headers='firstrow',tablefmt='psql'))

def Contrats_non_valides_pro():
    contrats_non_valides_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def contrats_non_valides_pro():
    print("---- Vous etes au menu d'affichage des contrats non valides ----\n\n")
    sql = "SELECT numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour, PrixDepassementKm,signe_par,\
    agent_com,vehicule,facture FROM ContratDeLocation_pro WHERE validation = FALSE ORDER BY numero"
    if not BDD().execute(sql):
        return;
    contrats = BDD().fetchall()
    print("---- Contrats non valides ----")
    contrats.insert(0,("numero","nbKmInit","nbKmFin","validation","cbClient","dateLoc","dateRetour"\
    ,"PrixDepassementKm","signe_par","agent_com","vehicule","facture"))
    print(tabulate(contrats,headers='firstrow',tablefmt='psql'))

def Valider_contrat_part():
    valider_contrat_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def valider_contrat_part():
    print("---- Vous etes au menu pour valider un contrat ----\n\n")
    contrats_non_valides_part()
    to_continue = Menu.choix_binaire("Voulez-vous continuer ? O/N ")
    if not to_continue:
        return
    numero_contrat = Menu.input_int("Quel est le numero du contrat a valider (taper break pour sortir) ? ")
    if numero_contrat == 'break':
        return
    Personnel.afficher_agents_com()
    id_agent_com = Menu.input_int("Saisissez ID de l agent commercial qui valide le contrat : ")
    sql = "UPDATE ContratDeLocation_part SET validation = TRUE , agent_com = "+str(id_agent_com)+" WHERE numero = "+str(numero_contrat)+";"
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Validation reussi ----")


def Valider_contrat_pro():
    valider_contrat_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def valider_contrat_pro():
    print("---- Vous etes au menu pour valider un contrat ----\n\n")
    contrats_non_valides_pro()
    to_continue = Menu.choix_binaire("Voulez-vous continuer ? O/N ")
    if not to_continue:
        return
    numero_contrat = Menu.input_int("Quel est le numero du contrat a valider ? ")
    Personnel.afficher_agents_com()
    id_agent_com = Menu.input_int("Saisissez ID de l agent commercial qui valide le contrat : ")
    sql = "UPDATE ContratDeLocation_pro SET validation = TRUE , agent_com = "+str(id_agent_com)+" WHERE numero = "+str(numero_contrat)+";"
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Validation reussi ----")
    

def Ajouter_contrat_pro():
    ajouter_contrat_pro()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()

def ajouter_contrat_pro():
    print("---- Vous etes au menu d'ajout de contrat professionel a la BDD ----\n\n")
    sql = "SELECT Max(numero) FROM ContratDeLocation_pro;"
    if not BDD().execute(sql):
        return;
    numero = BDD().fetchall()[0][0] + 1
    nbKmInit = Menu.input_float("Saisissez le nombre de Km au debut de la location : ")
    if Menu.choix_binaire("Voulez vous saisir le nombre de Km a la fin de la location ? O/N "):
        nbKmFin = Menu.input_float("Saisissez le nombre de Km a la fin de la location : ")
    else:
        nbKmFin = "NULL"

    validation = str("FALSE")
    cbClient = Menu.input_cb("Saisissez le numero de carte bancaire du client : (16 caracteres) ")
    dateLoc = Menu.input_date("Saisissez la date de debut de location (YYYY-MM-JJ) : ")
    if Menu.choix_binaire("Voulez vous saisir la date de fin de la location ? O/N "):
        dateRetour = """'{}'""".format(Menu.input_date("Saisissez la date de fin de la location (YYYY-MM-JJ) : "))
    else:
        dateRetour = "NULL"
    
    PrixDepassementKm = round(Menu.input_float("Saisissez le prix de depassement Km : "),2)
    Client.affiche_client_pro()
    signe_par = input("Entrez le nom du client professionel lie a ce contrat : ")
    Personnel.afficher_agents_com()
    agent_com = Menu.input_int("Entrez l id de l agent commercial lie au contrat : ")
    Voitures.parc_voiture()
    vehicule = Menu.input_plaque()
    Factures.affiche_facture_pro(signe_par)
    to_continue = Menu.choix_binaire("Si pas de facture dispo tapez N et creez une facture avant le contrat sinon tapez O")
    if not to_continue:
        return
    facture = Menu.input_int("Entrez ID de la facture associee a ce contrat : ")
    Client.affiche_conduct_pro(signe_par)
    conducteur = Menu.input_permis()

    sql = "INSERT INTO ContratDeLocation_pro (numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,\
    PrixDepassementKm,signe_par,agent_com,vehicule,facture,conducteur) VALUES ({},{},{},{},'{}','{}',{},{},'{}','{}','{}'\
    ,'{}','{}');".format(numero, nbKmInit, nbKmFin, validation, cbClient, dateLoc, dateRetour, PrixDepassementKm, signe_par, agent_com, vehicule, facture, conducteur)
    
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")

def Ajouter_contrat_part():
    ajouter_contrat_part()
    input("Appuyez sur une touche pour revenir au menu precedent...")
    Menu.clear()
    
def ajouter_contrat_part():
    print("---- Vous etes au menu d'ajout de contrat pour un particulier a la BDD ----\n\n")
    sql = "SELECT Max(numero) FROM ContratDeLocation_part;"
    if not BDD().execute(sql):
        return;
    numero = BDD().fetchall()[0][0] + 1
    nbKmInit = Menu.input_float("Saisissez le nombre de Km au debut de la location : ")
    if Menu.choix_binaire("Voulez vous saisir le nombre de Km a la fin de la location ? O/N "):
        nbKmFin = Menu.input_float("Saisissez le nombre de Km a la fin de la location : ")
    else:
        nbKmFin = "NULL"

    validation = str("FALSE")
    cbClient = Menu.input_cb("Saisissez le numero de carte bancaire du client : (16 caracteres) ")
    dateLoc = Menu.input_date("Saisissez la date de debut de location (YYYY-MM-JJ) : ")
    if Menu.choix_binaire("Voulez vous saisir la date de fin de la location ? O/N "):
        dateRetour = """'{}'""".format(Menu.input_date("Saisissez la date de fin de la location (YYYY-MM-JJ) : "))
    else:
        dateRetour = "NULL"
    
    PrixDepassementKm = round(Menu.input_float("Saisissez le prix de depassement Km : "),2)
    Client.affiche_client_particulier()
    print("Entrez le permis du client particulier lie a ce contrat : ")
    signe_par = Menu.input_permis()
    Personnel.afficher_agents_com()
    agent_com = Menu.input_int("Entrez l id de l agent commercial lie au contrat : ")
    Voitures.parc_voiture()
    vehicule = Menu.input_plaque()
    Factures.affiche_facture_part(signe_par)
    to_continue = Menu.choix_binaire("Si pas de facture dispo tapez N et creez une facture avant le contrat sinon tapez O")
    if not to_continue:
        return
    facture = Menu.input_int("Entrez ID de la facture associee a ce contrat : ")

    sql = "INSERT INTO ContratDeLocation_part (numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,\
    PrixDepassementKm,signe_par,agent_com,vehicule,facture) VALUES ({},{},{},{},'{}','{}',{},{},'{}','{}','{}'\
    ,'{}');".format(numero, nbKmInit, nbKmFin, validation, cbClient, dateLoc, dateRetour, PrixDepassementKm, signe_par, agent_com, vehicule, facture)
    
    if not BDD().execute(sql):
        return;
    BDD().commit()
    print("---- Ajout reussi ----")
