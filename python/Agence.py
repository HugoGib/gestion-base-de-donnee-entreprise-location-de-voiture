from BDD import BDD
from tabulate import tabulate
from Menu import Menu

def ajout_agence():
        print("---- Vous etes au menu d'ajout d'agence a la BDD ----\n\n")
        nom = input("Saisissez le nom de l'agence a ajouter : ")
        nom = nom[0].upper() + nom[1:]
        adresse = input("Saisissez l'adresse de l'agence a ajouter : ")
        
        agence_ajout_sql = "INSERT INTO Agence_location (adresse,nom) \
        VALUES ('{}','{}')".format(adresse,nom)
        
        if not BDD().execute(agence_ajout_sql):
            return;
        BDD().commit()
        print("---- Ajout reussi ----")
        
        agence_sql = "SELECT * FROM Agence_location WHERE nom = '{}'".format(nom)
        if not BDD().execute(agence_sql):
            return;
        agence = BDD().fetchall()
        agence.insert(0,("adresse","nom"))
        print(tabulate(agence,headers='firstrow',tablefmt='psql'))
        
        input("Appuyez sur une touche pour revenir au menu precedent...")
        Menu.clear()