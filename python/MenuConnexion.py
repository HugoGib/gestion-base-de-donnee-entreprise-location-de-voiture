from Menu import Menu
import psycopg2 as p
import getpass
import sys

class MenuConnexion(Menu):
    """Menu qui permet de se connecter a la base de donnee."""

    def __init__(self):
        """Renvoie un tuple (connexion,curseur)."""
        while True:
            print("Veuillez saisir votre host : (taper break pour quitter l'application)")
            self.host = input("host ? : ")
            if self.host == 'break':
                print("Arret de l'application")
                input("Tapez sur n'importe quelle touche pour quitter...")
                sys.exit()
            print("Veuillez saisir votre dbname : ")
            self.dbname = input("dbname : ")
            print("Veuillez saisir votre username : ")
            self.username = input("username ? : ")
            print("Veuillez saisir votre password :")
            self.password = getpass.getpass("password ? :")

            print("\n\n\n---- Connexion a la BDD.... ----\n")
            try :
                self.connexion = p.connect("host=%s dbname=%s user=%s password=%s" \
                         %(self.host,self.dbname,self.username,self.password))
                Menu.clear()
                print("---- Connexion reussie ----\n\n")
                break
            except p.OperationalError as e:
                print("\n/!\ Erreur /!\ \nMessage systeme : ",e)
                print("Si message systeme vide : identifiant(s) incorrect(s)")
