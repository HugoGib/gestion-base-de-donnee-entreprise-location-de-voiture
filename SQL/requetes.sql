/*Véhicules dipsonibles*/

select categorie, nom, marque, nbportes, couleur, nbkm, typecarbu, tarif, numero_immat
	from
(
	select categorie, nom, marque, nbportes, couleur, nbkm, typecarbu, tarif, numero_immat
	from vehicule v inner join modele m on v.modele = m.nom
except
(
	select distinct categorie, nom, marque, nbportes, couleur, nbkm, typecarbu, tarif, numero_immat
	from contratdelocation_part c_pa
	 inner join vehicule v on c_pa.vehicule = v.numero_immat
	 inner join modele m on v.modele = m.nom
	where c_pa.dateretour < now()
union
	select distinct categorie, nom, marque, nbportes, couleur, nbkm, typecarbu, tarif, numero_immat
	from contratdelocation_pro c_pro
	 inner join vehicule v on c_pro.vehicule = v.numero_immat
	 inner join modele m on v.modele = m.nom
	where c_pro.dateretour < now()
)) res order by categorie asc, nom asc;

/*stat par client*/

select nom, sum(montant) as total from
(
	select c.nom as nom, montant
	from facture_particulier f inner join client_particulier c on f.paye_par = c.numero_permis
	union
	select paye_par as nom, montant
	from facture_pro

)factures group by nom order by nom;


/*stat par véhicule*/

select vehicule, total from
(
select c_p.vehicule as vehicule, sum(f.montant) as total
from contratdelocation_part c_p
inner join facture_particulier f on c_p.facture = f.numero
group by vehicule
union
select c_p.vehicule as vehicule, sum(v.tarif *(c_p.dateretour - c_p.dateloc)) as total
from contratdelocation_pro c_p inner join vehicule v on c_p.vehicule = v.numero_immat
where (c_p.nbkmfin - c_p.nbkminit) <=100
group by c_p.vehicule
union
select c_p.vehicule as vehicule,
	sum(v.tarif *(c_p.dateretour - c_p.dateloc)+(c_p.nbkmfin - c_p.nbkminit -100)*c_p.prixdepassementkm) as total
from contratdelocation_pro c_p inner join vehicule v on c_p.vehicule = v.numero_immat
where (c_p.nbkmfin - c_p.nbkminit) >100
group by c_p.vehicule
) stat_vehicule order by total desc;

/*stat par modelet*/
select modele, sum(total) from
(
select v.modele as modele, sum(f.montant) as total
from contratdelocation_part c_p
inner join facture_particulier f on c_p.facture = f.numero
inner join vehicule v on c_p.vehicule = v.numero_immat
group by v.modele
union
select v.modele as modele, sum(v.tarif *(c_p.dateretour - c_p.dateloc)) as total
from contratdelocation_pro c_p inner join vehicule v on c_p.vehicule = v.numero_immat
where (c_p.nbkmfin - c_p.nbkminit) <=100
group by v.modele
union
select v.modele as modele,
	sum(v.tarif *(c_p.dateretour - c_p.dateloc)+(c_p.nbkmfin - c_p.nbkminit -100)*c_p.prixdepassementkm) as total
from contratdelocation_pro c_p inner join vehicule v on c_p.vehicule = v.numero_immat
where (c_p.nbkmfin - c_p.nbkminit) >100
group by v.modele
) stat_vehicule group by modele order by modele;

/*Stat par catégorie*/

select categorie, sum(total )as total from
(
select m.categorie as categorie, sum(f.montant) as total
from contratdelocation_part c_p
inner join facture_particulier f on c_p.facture = f.numero
inner join vehicule v on c_p.vehicule = v.numero_immat
inner join modele m on v.modele = m.nom
group by m.categorie
union
select m.categorie as categorie, sum(v.tarif *(c_p.dateretour - c_p.dateloc)) as total
from contratdelocation_pro c_p
inner join vehicule v on c_p.vehicule = v.numero_immat
inner join modele m on v.modele = m.nom
where (c_p.nbkmfin - c_p.nbkminit) <=100
group by m.categorie
union
select m.categorie as categorie,
	sum(v.tarif *(c_p.dateretour - c_p.dateloc)+(c_p.nbkmfin - c_p.nbkminit -100)*c_p.prixdepassementkm) as total
from contratdelocation_pro c_p
inner join vehicule v on c_p.vehicule = v.numero_immat
inner join modele m on v.modele = m.nom
where (c_p.nbkmfin - c_p.nbkminit) >100
group by m.categorie
) stat_vehicule group by categorie order by categorie;

/*vehicules en attente de controle*/

select distinct v.numero_immat, v.couleur, v.tarif, v.modele, v.typecarbu, v.agence
from controles c left outer join contratdelocation_pro c_pr on c.contrat_loc_pro = c_pr.numero
left outer join contratdelocation_part c_pa on c.contrat_loc_part = c_pa.numero
left outer join vehicule v on c_pr.vehicule = v.numero_immat or c_pa.vehicule = v.numero_immat
where c.date_realisation is null;
