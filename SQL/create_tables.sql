CREATE TABLE IF NOT EXISTS Categorie(
	nom VARCHAR(20) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Modele(
	nom VARCHAR(50) PRIMARY KEY,
	marque VARCHAR(50) NOT NULL,
	nbPortes INT CHECK (nbPortes >=0) NOT NULL,
	categorie VARCHAR(20) REFERENCES Categorie(nom) NOT NULL
);

CREATE TABLE IF NOT EXISTS TypeCarburant(
	carburant VARCHAR(20) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Agence_location(
	adresse VARCHAR(255) PRIMARY KEY,
	nom VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Vehicule (
	numero_immat VARCHAR(7) PRIMARY KEY,
	couleur VARCHAR(20) NOT NULL,
	nbkm INT CHECK (nbkm >=0) NOT NULL,
	tarif FLOAT CHECK (tarif >=0) NOT NULL,
	modele VARCHAR(50) REFERENCES Modele(nom) NOT NULL,
	typeCarbu VARCHAR(20) REFERENCES TypeCarburant(carburant) NOT NULL,
	loue BOOLEAN NOT NULL,
	agence VARCHAR(255) REFERENCES Agence_location(adresse) NOT NULL
);

CREATE TABLE IF NOT EXISTS Option_tab(
	nom VARCHAR(50) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Societe_entretien(
	nom VARCHAR(50) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Option_vehicule(
	option_v VARCHAR(50) REFERENCES Option_tab(nom) NOT NULL,
	vehicule VARCHAR(7) REFERENCES Vehicule(numero_immat) NOT NULL
);

CREATE TABLE IF NOT EXISTS Societe_embauche(
	societe_entretien VARCHAR(50) REFERENCES Societe_entretien(nom) NOT NULL,
	agence VARCHAR(255) REFERENCES Agence_location(adresse) NOT NULL
);



CREATE TABLE IF NOT EXISTS Agent_technique(
	id INT PRIMARY KEY CHECK (id>=1),
	nom VARCHAR(50) NOT NULL,
	prenom VARCHAR(50) NOT NULL,
	agence VARCHAR(255) REFERENCES Agence_location(adresse) NOT NULL
);

CREATE TABLE IF NOT EXISTS Agent_commercial(
	id INT PRIMARY KEY CHECK (id>=1),
	nom VARCHAR(50) NOT NULL,
	prenom VARCHAR(50) NOT NULL,
	agence VARCHAR(255) REFERENCES Agence_location(adresse) NOT NULL
);

CREATE TABLE IF NOT EXISTS Moyen_paiement(
	nom VARCHAR(20) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Client_particulier(
	numero_permis VARCHAR(25) PRIMARY KEY,
	nom VARCHAR(50) NOT NULL,
	prenom VARCHAR(50) NOT NULL,
	adresse VARCHAR(255) NOT NULL,
	numero_tel VARCHAR(11) UNIQUE NOT NULL,
	ddn DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS Facture_particulier(
	numero INT PRIMARY KEY CHECK (numero>=1),
	montant FLOAT CHECK (montant >=0) NOT NULL,
	payee BOOL CHECK ((date_paiement != NULL AND payee = True AND paiement != NULL)
					  OR (payee = False AND date_paiement = NULL AND paiement = NULL)) NOT NULL,
	date_paiement DATE,
	paiement VARCHAR(20) REFERENCES Moyen_paiement(nom),
	agent_com INT REFERENCES Agent_commercial(id) NOT NULL,
	paye_par VARCHAR(25) REFERENCES Client_particulier(numero_permis) NOT NULL
);

CREATE TABLE IF NOT EXISTS Client_pro(
	nom VARCHAR(50) PRIMARY KEY,
	info_ets TEXT
);

CREATE TABLE IF NOT EXISTS Facture_pro(
	numero INT PRIMARY KEY CHECK (numero>=1),
	montant FLOAT CHECK (montant >=0) NOT NULL,
	payee BOOL CHECK ((date_paiement != NULL AND payee = True AND paiement != NULL)
					  OR (payee = False AND date_paiement = NULL AND paiement = NULL)) NOT NULL,
	date_paiement DATE,
	paiement VARCHAR(20) REFERENCES Moyen_paiement(nom),
	agent_com INT REFERENCES Agent_commercial(id) NOT NULL,
	paye_par VARCHAR(50) REFERENCES Client_pro(nom) NOT NULL
);

CREATE TABLE IF NOT EXISTS Conducteur(
	numero_permis VARCHAR(25) PRIMARY KEY,
	nom VARCHAR(50) NOT NULL,
	prenom VARCHAR(50) NOT NULL,
	ddn DATE NOT NULL,
	entreprise VARCHAR(50) REFERENCES Client_pro(nom) NOT NULL
);

CREATE TABLE IF NOT EXISTS ContratDeLocation_pro(
	numero INT PRIMARY KEY CHECK(numero>=1),
	nbKmInit FLOAT CHECK (nbKmInit>=0) NOT NULL,
	nbKmFin FLOAT CHECK ((nbKmFin>=nbKmInit AND dateRetour != NULL) OR nbKmFin = NULL),
	validation BOOL NOT NULL,
	cbClient VARCHAR(25) NOT NULL,
	dateLoc DATE NOT NULL,
	dateRetour DATE CHECK (dateRetour>=dateLoc OR dateRetour = NULL),
	PrixDepassementKm FLOAT CHECK (PrixDepassementKm >=0) NOT NULL,
	signe_par VARCHAR(50) REFERENCES Client_pro(nom) NOT NULL,
	agent_com INT REFERENCES Agent_commercial(id) NOT NULL,
	vehicule VARCHAR(7) REFERENCES Vehicule(numero_immat) NOT NULL,
	facture INT REFERENCES Facture_pro(numero),
	conducteur VARCHAR(25) REFERENCES Conducteur(numero_permis) NOT NULL
);

CREATE TABLE IF NOT EXISTS ContratDeLocation_part(
	numero INT PRIMARY KEY CHECK(numero>=1),
	nbKmInit FLOAT CHECK (nbKmInit>=0) NOT NULL,
	nbKmFin FLOAT CHECK (nbKmFin>=nbKmInit OR nbKmFin = NULL),
	validation BOOL NOT NULL,
	cbClient VARCHAR(25) NOT NULL,
	dateLoc DATE NOT NULL,
	dateRetour DATE CHECK (dateRetour>=dateLoc OR dateRetour = NULL),
	PrixDepassementKm FLOAT CHECK (PrixDepassementKm >=0) NOT NULL,
	signe_par VARCHAR(25) REFERENCES Client_particulier(numero_permis) NOT NULL,
	agent_com INT REFERENCES Agent_commercial(id) NOT NULL,
	vehicule VARCHAR(7) REFERENCES Vehicule(numero_immat) NOT NULL,
	facture INT REFERENCES Facture_particulier(numero)
);

CREATE TABLE IF NOT EXISTS Controles(
	numero INT PRIMARY KEY CHECK(numero >=1),
	contrat_loc_part INT REFERENCES ContratDeLocation_part(numero),
	contrat_loc_pro INT REFERENCES ContratDeLocation_pro(numero),
	CHECK ((contrat_loc_part = NULL AND contrat_loc_pro != NULL)
		   OR (contrat_loc_part != NULL AND contrat_loc_pro = NULL)),
	date_realisation DATE
);

CREATE TABLE IF NOT EXISTS Controle_societe(
	controle INT REFERENCES Controles(numero) NOT NULL,
	societe_entretien VARCHAR(50) REFERENCES Societe_entretien(nom) NOT NULL
);

CREATE TABLE IF NOT EXISTS Controle_agent(
	controle INT REFERENCES Controles(numero) NOT NULL,
	agent INT REFERENCES Agent_technique(id) NOT NULL
);
