INSERT INTO Categorie VALUES ('citadine'),('berline petite'),
('berline moyenne'),('berline grande'),('4x4 SUV'),('break'),
('pickup'),('utilitaire');

INSERT INTO Modele (nom,marque,nbPortes,categorie) VALUES
('clio 4','renault',5,'berline petite'),
('twingo 3','renault',5,'citadine'),
('tiguan','volkswagen',5,'4x4 SUV'),
('208','peugeot',5,'berline petite'),
('a4','audi',5,'break'),
('classe E','mercedes-benz',5,'break'),
('ranger','ford',4,'pickup'),
('kangoo','renault',4,'utilitaire');

INSERT INTO TypeCarburant VALUES ('SP95'),('SP98'),('E85'),
('Diesel'),('GPL'),('Electrique');

INSERT INTO Agence_location (adresse,nom) VALUES
('10 avenue des Champs Elysees, 75000, Paris','Agence des Champs'),
('12 rue du general de Gaulle, 60200, Compiegne', 'Super Locations');

INSERT INTO Vehicule (numero_immat,couleur,nbkm,tarif,modele,typeCarbu,loue,agence) VALUES
('AA001AA','blanc',15240,37.49,'clio 4','SP95',FALSE,'10 avenue des Champs Elysees, 75000, Paris'),
('BB001BB','rouge',1036,25.89,'twingo 3','Diesel',TRUE,'10 avenue des Champs Elysees, 75000, Paris'),
('CC001CC','beige',48520,85.89,'tiguan','Diesel',FALSE,'10 avenue des Champs Elysees, 75000, Paris'),
('DD001DD','gris',89563,42.56,'208','SP98',TRUE,'10 avenue des Champs Elysees, 75000, Paris'),
('EE001EE','noir',12389,68.37,'a4','SP98',FALSE,'10 avenue des Champs Elysees, 75000, Paris'),
('FF001FF','noir',241658,89.23,'classe E','Diesel',FALSE,'10 avenue des Champs Elysees, 75000, Paris'),
('GG001GG','blanc',254368,101.32,'ranger','E85',FALSE,'10 avenue des Champs Elysees, 75000, Paris'),
('HH001HH','blanc',24130,52.14,'kangoo','Electrique',TRUE,'10 avenue des Champs Elysees, 75000, Paris');

INSERT INTO Option_tab VALUES ('bluetooth'),('gps'),('radar de recul'),
('radar avant'),('regulateur'),('limitateur'),('conduite eco'),('alarme'),
('start and stop'),('clef sans contact');



INSERT INTO agent_commercial(id,nom,prenom,agence) VALUES
(1,'Leroux','Thibaut','10 avenue des Champs Elysees, 75000, Paris'),
(2,'Gauthier','Gaston','10 avenue des Champs Elysees, 75000, Paris'),
(3,'Mougeot','Beatrice','10 avenue des Champs Elysees, 75000, Paris');

INSERT INTO agent_technique(id,nom,prenom,agence) VALUES
(1,'Tyson','Mac','10 avenue des Champs Elysees, 75000, Paris'),
(2,'Lebron','James','10 avenue des Champs Elysees, 75000, Paris'),
(3,'Zinedin','Zidane','10 avenue des Champs Elysees, 75000, Paris');

INSERT INTO societe_entretien(nom) VALUES
('Carglass'),('Renaut'),('Skoda'),('Audi'),('Honda');


INSERT INTO Moyen_paiement(nom) VALUES
('espece'),('Carte Bancaire'),('Cheque'),('Bitcoin');


INSERT INTO Option_vehicule (vehicule,option_v) VALUES
('AA001AA','bluetooth'),('AA001AA','gps'),
('AA001AA','radar de recul'),('AA001AA','radar avant'),
('AA001AA','regulateur'),('AA001AA','limitateur'),
('AA001AA','conduite eco'),('AA001AA','clef sans contact'),

('BB001BB','bluetooth'),('BB001BB','gps'),
('BB001BB','radar de recul'),('BB001BB','regulateur'),
('BB001BB','limitateur'),

('CC001CC','bluetooth'),('CC001CC','gps'),
('CC001CC','radar de recul'),('CC001CC','radar avant'),
('CC001CC','regulateur'),('CC001CC','limitateur'),
('CC001CC','start and stop'),

('DD001DD','bluetooth'),('DD001DD','gps'),
('DD001DD','radar de recul'),('DD001DD','radar avant'),
('DD001DD','regulateur'),('DD001DD','limitateur'),
('DD001DD','conduite eco'),('DD001DD','clef sans contact'),

('EE001EE','bluetooth'),('EE001EE','gps'),
('EE001EE','radar de recul'),('EE001EE','radar avant'),
('EE001EE','regulateur'),('EE001EE','limitateur'),
('EE001EE','conduite eco'),('EE001EE','clef sans contact'),

('FF001FF','bluetooth'),('FF001FF','gps'),
('FF001FF','radar de recul'),('FF001FF','regulateur'),
('FF001FF','limitateur'),

('GG001GG','bluetooth'),('GG001GG','gps'),
('GG001GG','radar de recul'),('GG001GG','radar avant'),
('GG001GG','regulateur'),('GG001GG','limitateur'),
('GG001GG','start and stop'),

('HH001HH','bluetooth'),('HH001HH','gps'),
('HH001HH','radar de recul'),('HH001HH','radar avant'),
('HH001HH','regulateur'),('HH001HH','limitateur'),
('HH001HH','conduite eco'),('HH001HH','clef sans contact');


INSERT INTO Client_particulier (numero_permis,nom,prenom,adresse,numero_tel,ddn) VALUES
('582804825417OU','Fernandez','Burton','9234 Nibh Av.','058924352','1957-04-21'),
('893414618509VE','Carroll','Emma','7081 Curae; Av.','035349250','1995-03-18'),
('359286413841PB','Reed','Price','Appartement 222-9360 Est Impasse','032329124','1982-07-29');

INSERT INTO Client_pro (nom,info_ets) VALUES 
('Bob le bricoleur','Plombier Paris 12-13eme'),
('APA','aide aux personnes agees'),
('Les demenageurs parisiens',''),
('Elisa Martin','coach sportif');

INSERT INTO Conducteur (numero_permis,nom,prenom,ddn,entreprise) VALUES
('958210766252PS','Levine','Jenette','1954-04-01','Bob le bricoleur'),
('742678479011DR','Joyce','Faith','1989-05-29','APA'),
('214802202922NL','Kim','Jason','1997-06-20','Bob le bricoleur'),
('116195506687ER','Leblanc','Gay','1984-09-21','APA'),
('152188515385WJ','Blake','Kasper','1966-10-19','APA'),
('917459329586FT','Sandoval','Petra','1974-06-05','APA'),
('727448264084LP','Hardin','Maggie','1984-03-22','Bob le bricoleur'),
('719744322165XF','Conley','Dillon','1962-07-20','APA'),
('209961001370FL','Simmons','Zephania','1987-05-03','Bob le bricoleur'),
('020137495061SW','Underwood','Kirby','1975-01-14','APA'),
('958741915911NB','Ballard','Lisandra','1987-04-11','Bob le bricoleur'),
('249758998183DM','Morin','Savannah','1970-01-30','Bob le bricoleur'),
('228696793832QR','Armstrong','Dominic','1970-12-20','Les demenageurs parisiens'),
('447881399264NU','Chase','Megan','1982-01-29','Les demenageurs parisiens'),
('185959974699TO','Martin','Elisa','1972-07-18','Elisa Martin');

INSERT INTO Facture_pro(numero,montant,payee,date_paiement,paiement,agent_com,paye_par) VALUES
(1,2576.7,TRUE,'10-05-10','Carte Bancaire',2,'Bob le bricoleur'),
(2,626.15,FALSE,NULL,NULL,1,'Les demenageurs parisiens') ;

INSERT INTO ContratDeLocation_pro(numero,nbKmInit,nbKmFin,validation,cbClient,dateLoc,dateRetour,
PrixDepassementKm,signe_par,agent_com,vehicule,facture,conducteur) VALUES
(1,15240,NULL,TRUE,'5440501234567811','2021-05-03','2021-06-03',1,'Elisa Martin',1,'AA001AA',NULL,'185959974699TO'),
(2,1036,NULL,TRUE,'5440501234567822','2021-04-03','2021-10-12',0.5,'Bob le bricoleur',3,'BB001BB',NULL,'209961001370FL'),
(3,48420,48520,TRUE,'5440501234567822','2021-04-03','2021-05-03',1, 'Bob le bricoleur',3,'CC001CC',1,'249758998183DM'),
(4,89563,NULL,FALSE,'5440501234567833','2021-05-03','2021-12-01',0.8,'APA',2,'DD001DD',NULL,'020137495061SW'),
(5,254068,254368,TRUE,'5440501234567844','2021-05-03','2021-05-07',0.9,'Les demenageurs parisiens',1,'FF001FF',2,'228696793832QR');

INSERT INTO facture_particulier(numero, montant, payee , date_paiement, paiement, agent_com, paye_par) VALUES 
(1,1200,TRUE, '2021-01-22','Carte Bancaire',1,'582804825417OU'),
(2,1124.7,FALSE, NULL,NULL,1,'893414618509VE'),
(3,1564.2,TRUE,'2020-04-17','Cheque',2,'359286413841PB');

INSERT INTO ContratDeLocation_part VALUES 
(1, 13000.4,13500.6,TRUE,'1638934127560987','2020-11-30','2020-12-15',0.1,'582804825417OU',1,'EE001EE',1),
(2, 11024.6,11124.6,TRUE,'5345672453667977','2021-01-04','2021-02-04',0.1,'893414618509VE',1,'AA001AA',2),
(3,24030,24130,TRUE,'4979672453667944','2020-03-15','2020-04-15',0.1,'359286413841PB',3,'HH001HH',3),
(4,24130 ,NULL,TRUE,'1638934127560987','2020-05-10',NULL,0.1,'582804825417OU',1,'HH001HH',NULL);

INSERT INTO Societe_Embauche VALUES 
('Audi','10 avenue des Champs Elysees, 75000, Paris'),
('Carglass','10 avenue des Champs Elysees, 75000, Paris'),
('Carglass','12 rue du general de Gaulle, 60200, Compiegne'),
('Renaut','12 rue du general de Gaulle, 60200, Compiegne');

INSERT INTO controles(numero,contrat_loc_part, date_realisation) VALUES 
(1, 1, '2020-12-17'),(2, 2, '2021-01-14'),
(3, 3, '2020-04-21'), (4, 4, NULL);

INSERT INTO controles(numero,contrat_loc_pro, date_realisation) VALUES 
(5, 1, '2021-06-13'), (6, 2, '2021-10-29'),
(7, 3, NULL), (8, 4, NULL),
(9, 5, NULL);

INSERT INTO Controle_agent VALUES 
(1,2),(2,3),(3,1),(4,3),(5,1),(6,2),(7,2);




